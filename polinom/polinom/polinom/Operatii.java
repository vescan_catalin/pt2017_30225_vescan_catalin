package polinom;

import java.util.ArrayList;

public class Operatii{	
	
	int contor = -1;
	
	ArrayList<Monom> polinom1 = new ArrayList<Monom>();
	ArrayList<Monom> polinom2 = new ArrayList<Monom>();
	ArrayList<Monom> polinomr = new ArrayList<Monom>();
	ArrayList<Monom> auxiliar = new ArrayList<Monom>();
	
	Monom monom;
	
	public ArrayList<Monom> adunare(ArrayList<Monom> polinom1, ArrayList<Monom> polinom2){
		
		// initializare polinom rezultat cu val din polinom1
		if(polinom1.get(0).exp >= polinom2.get(0).exp){
			for(int i = 0; i < polinom1.size(); ++i)
				polinomr.add(polinom1.get(i));	
			
			// adunare polinom2 la polinom rezultat = polinom1
			for(int i = 0; i < polinom2.size(); ++i)
				for(int j = 0; j < polinom1.size(); ++j)
					if(polinom2.get(i).exp == polinom1.get(j).exp)
						polinomr.get(j).coeficient += polinom2.get(i).coeficient;

			// inserarea monoamelor din polinomul2 care nu se gasesc in polinomul1
			for(int i = 0; i < polinom2.size(); ++i)
				for(int j = 0; j < polinom1.size() - 1; ++j)	
					if(polinom2.get(i).exp < polinom1.get(j).exp && polinom2.get(i).exp > polinom1.get(j + 1).exp)
						// inserez monomul lipsa
						polinomr.add(polinom1.size() - 2, polinom2.get(i));
		}
		else{
			// initializare polinom rezultat cu val din polinom2
			for(int k = 0; k < polinom2.size(); ++k)
				polinomr.add(polinom2.get(k));
			
			// adunare polinom1 la polinom rezultat = polinom2
			for(int k = 0; k < polinom1.size(); ++k)
				for(int q = 0; q < polinom2.size(); ++q)
					if(polinom1.get(k).exp == polinom2.get(q).exp)
						polinomr.get(q).coeficient += polinom1.get(k).coeficient;

			// inserarea monoamelor din polinomul1 care nu se gasesc in polinomul2
			for(int k = 0; k < polinom1.size(); ++k)
				for(int q = 0; q < polinom2.size() - 1; ++q)	
					if(polinom1.get(k).exp < polinom2.get(q).exp && polinom1.get(k).exp > polinom2.get(q + 1).exp)
						// inserez monomul lipsa
						polinomr.add(polinom2.size() - 2, polinom1.get(k));
		}
		
		return polinomr;
	}

	public ArrayList<Monom> scadere(ArrayList<Monom> polinom1, ArrayList<Monom> polinom2){
		
		ArrayList<Monom> temp = new ArrayList<Monom>();
		
		if(polinom1.size() >= polinom2.size()){
			// initializare polinom rezultat cu val din polinom1
			for(int i = 0; i < polinom1.size(); ++i)
				temp.add(polinom1.get(i));
			
			// scadere polinom2 din polinom rezultat = polinom1
			for(int i = 0; i < polinom2.size(); ++i)
				for(int j = 0; j < polinom1.size(); ++j)
					if(polinom2.get(i).exp == polinom1.get(j).exp)
						temp.get(j).coeficient -= polinom2.get(i).coeficient;

			// inserarea monoamelor din polinomul2 care nu se gasesc in polinomul1
			for(int i = 0; i < polinom2.size(); ++i)
				for(int j = 0; j < polinom1.size() - 1; ++j)	
					if(polinom2.get(i).exp < polinom1.get(j).exp && polinom2.get(i).exp > polinom1.get(j + 1).exp)
						// inserez monomul lipsa
						temp.add(polinom1.size() - 2, polinom2.get(i));
		}
		else{
			// initializare polinom rezultat cu val din polinom2
			for(int k = 0; k < polinom2.size(); ++k)
				temp.add(polinom2.get(k));
	
			// scadere polinom1 din polinom rezultat = polinom2
			for(int k = 0; k < polinom1.size(); ++k)
				for(int q = 0; q < polinom2.size(); ++q)
					if(polinom1.get(k).exp == polinom2.get(q).exp)
						temp.get(q).coeficient -= polinom1.get(k).coeficient;
	
			// inserarea monoamelor din polinomul1 care nu se gasesc in polinomul2
			for(int k = 0; k < polinom1.size(); ++k)
				for(int q = 0; q < polinom2.size() - 1; ++q)	
					if(polinom1.get(k).exp < polinom2.get(q).exp && polinom1.get(k).exp > polinom2.get(q + 1).exp)
						// inserez monomul lipsa
						temp.add(polinom2.size() - 2, polinom1.get(k));
		}
		
		return temp;
	}
	
	public ArrayList<Monom> inmultire(ArrayList<Monom> polinom1, ArrayList<Monom> polinom2){
		
		//polinomr.clear();
		ArrayList<Monom> temp = new ArrayList<Monom>();
		
		// produsul monoamelor
		for(int i = 0; i < polinom1.size(); ++i)
			for(int j = 0; j < polinom2.size(); ++j){
				monom = new Monom();
				monom.coeficient = polinom1.get(i).coeficient * polinom2.get(j).coeficient;
				monom.exp = polinom1.get(i).exp + polinom2.get(j).exp;
				temp.add(monom);
			}
		
		// sortare polinom rezultat dupa exponent, descrescator
		temp.sort((x1,x2)->x2.exp-x1.exp);
		
		// adunare monoame cu exponent egal
		for( int i = 0; i < temp.size() - 1; ++i)
			if(temp.get(i).exp == temp.get(i + 1).exp)
				temp.get(i).coeficient += temp.get(i + 1).coeficient;
	
		return temp;		
	}

	public ArrayList<Monom> inmultireCuScalar(int scalar, ArrayList<Monom> polinom1) {
	
		for(int i = 0; i < polinom1.size(); ++i)
			polinomr.add(polinom1.get(i));
		
		for(int i = 0; i < polinom1.size(); ++i)
			polinomr.get(i).coeficient *= scalar;
				
		return polinomr;
	}

	public ArrayList<Monom> impartire(ArrayList<Monom> polinom1, ArrayList<Monom> polinom2){
		// 12x^4+6x^3+2x^1+7x^0 : 3x^2-1x^1+2x^0 = 4x^2+3x^1
		//-12x^4+4x^3-8x^2+2x^1+7x^0
		//-----------------------
		// 10x^3-8x^2+2x^1+7x^0
		// -9x^3+3x^2+6x^1+7x^0
		//-----------------------
		//1x^3-5x^2+8x^1+14x^0
		
		monom = new Monom();	
		polinomr.clear();
		
		contor++;
		if(polinom1.get(contor).exp >= polinom2.get(0).exp && polinom2.get(0).coeficient != 0){
			
			int raportCoef = polinom1.get(contor).coeficient / polinom2.get(0).coeficient;
			int exponent = polinom1.get(contor).exp - polinom2.get(0).exp;
			
			if(raportCoef != 0){
				//polinomr.clear();
				monom.coeficient = raportCoef;
				monom.exp = exponent;
				
				auxiliar.add(monom);
				polinomr.add(monom);
				this.polinom1 = scadere(polinom1, inmultire(polinomr, polinom2));
				impartire(this.polinom1, polinom2);
			}
		}
		
		return auxiliar;
	}
	
	public ArrayList<Monom> integrare(ArrayList<Monom> polinom1){
		
		for(int i = 0; i < polinom1.size(); ++i)
			polinomr.add(polinom1.get(i));
		
		for(int i = 0; i < polinomr.size()-1; ++i){
			polinomr.get(i).exp += 1 ;
			polinomr.get(i).coef = (double) 1 / polinomr.get(i).exp;
		}
		
		return polinomr;
	}
	
	public ArrayList<Monom> derivare(ArrayList<Monom> polinom1){
		
		for(int i = 0; i < polinom1.size(); ++i)
			polinomr.add(polinom1.get(i));
		for(int i = 0; i < polinomr.size(); ++i){
			polinomr.get(i).coeficient *= polinomr.get(i).exp; 
			polinomr.get(i).exp -= 1;
		}
		
		return polinomr;
	}
	
	public void spargere(String pol1, String pol2){
		
		pol1 = pol1.replaceAll("-", "+-");

		String[] mon1 = pol1.split("\\+");
		String[] auxiliar1 = null;

		for(String x : mon1){
			if(x.equals(""))	
				continue;
			auxiliar1 = x.split("x\\^");
			monom = new Monom();
			monom.coeficient = Integer.parseInt(auxiliar1[0]);
			monom.exp = Integer.parseInt(auxiliar1[1]);
			polinom1.add(monom);
		}
		
		pol2 = pol2.replaceAll("-", "+-");
		
		String[] mon2 = pol2.split("\\+");
		String[] auxiliar2 = null;

		for(String x : mon2){
			if(x.equals(""))	
				continue;
			auxiliar2 = x.split("x\\^");
			monom = new Monom();
			monom.coeficient = Integer.parseInt(auxiliar2[0]);
			monom.exp = Integer.parseInt(auxiliar2[1]);
			polinom2.add(monom);
		}
	}
	
	public void spargere1(String pol2){
				
		pol2 = pol2.replaceAll("-", "+-");
		
		String[] mon2 = pol2.split("\\+");
		String[] auxiliar2 = null;

		for(String x : mon2){
			if(x.equals(""))	
				continue;
			auxiliar2 = x.split("x\\^");
			monom = new Monom();
			monom.coeficient = Integer.parseInt(auxiliar2[0]);
			monom.exp = Integer.parseInt(auxiliar2[1]);
			polinom2.add(monom);
		}
	}
		
	// afisarea polinomului	
	public String toString(ArrayList<Monom> polinom){
		String x = "";
		for(int i = 0 ; i < polinom.size(); ++i){
			if(i == polinom.size() - 1){
				x = x + polinom.get(i).coeficient +"x^" +polinom.get(i).exp;
				return x;
			}
			x = x + polinom.get(i).coeficient +"x^" +polinom.get(i).exp +"+";	
		}
		return x;
	}

	public String toString2(ArrayList<Monom> polinom){
		String x = "";
		for(int i = 0 ; i < polinom.size(); ++i){
			if(i == polinom.size() - 1){
				x = x + polinom.get(i).coef +"x^" +polinom.get(i).exp;
				return x;
			}
			x = x + polinom.get(i).coef +"x^" +polinom.get(i).exp +"+";	
		}
		return x;
		
	}
	
}
