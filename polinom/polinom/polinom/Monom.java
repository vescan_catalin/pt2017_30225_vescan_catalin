package polinom;

public class Monom{
	
	public int coeficient, exp;
	public double coef;
	
	public int getCoeficient(){
		return coeficient;
	}
	
	public int getExp(){
		return exp;
	}
	
	public double getCoef(){
		return coef;
	}
	
}
