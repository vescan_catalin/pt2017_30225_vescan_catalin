package polinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Main{
	
	JFrame frame;
	JPanel panel;
	JLabel polinom1, polinom2 , polinomr, polinomc;
	JTextField txt_pol1, txt_pol2, txt_rezultat, txt_rezultat_rest;
	JButton adunare, scadere, inmultire, inmultireScalar, impartire, derivare, integrare;
	
	Operatii operatii = new Operatii();
	
	public Main(){
		// window
		frame = new JFrame(" Polinom ");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		
		// panel
		panel = new JPanel();
		panel.setLayout(null);
		
		// labels
		polinom1 = new JLabel("polinomul 1 :");
		polinom1.setBounds(50, 30, 100, 20);
		polinom2 = new JLabel("polinomul 2 :");
		polinom2.setBounds(400, 30, 100, 20);
		polinomr = new JLabel("polinomul rezultat :");
		polinomr.setBounds(450, 250, 200, 20);
		polinomc = new JLabel("restul :");
		polinomc.setBounds(450, 350, 200, 20);
		
		// text fields
		txt_pol1 = new JTextField();
		txt_pol1.setBounds(150, 30, 200, 20);
		txt_pol2 = new JTextField();
		txt_pol2.setBounds(500, 30, 200, 20);
		txt_rezultat = new JTextField();
		txt_rezultat.setBounds(250, 300, 450, 30);
		txt_rezultat.setEditable(false);
		txt_rezultat_rest = new JTextField();
		txt_rezultat_rest.setBounds(250, 400, 400, 30);
		txt_rezultat_rest.setEditable(false);
		
		// buttons
		adunare = new JButton("adunare");
		adunare.setBounds(50, 100, 100, 50);
		scadere = new JButton("scadere");
		scadere.setBounds(50, 200, 100, 50);
		inmultire = new JButton("inmultire");
		inmultire.setBounds(50, 300, 100, 50);
		inmultireScalar = new JButton("inmultire cu scalar");
		inmultireScalar.setBounds(200, 100, 150, 50);
		impartire = new JButton("impartire");
		impartire.setBounds(50, 400, 100, 50);
		derivare = new JButton("derivare");
		derivare.setBounds(200, 200, 100, 50);
		integrare = new JButton("integrare");
		integrare.setBounds(50, 500, 100, 50);
		
		// event on buttons
		adunare.addActionListener(new ActionEvents());
		scadere.addActionListener (new ActionEvents());
		inmultire.addActionListener(new ActionEvents());
		inmultireScalar.addActionListener(new ActionEvents());
		impartire.addActionListener(new ActionEvents());
		derivare.addActionListener(new ActionEvents());
		integrare.addActionListener(new ActionEvents());
		
		// add labels and text fields in panel
		panel.add(polinom1);	
		panel.add(polinom2);
		panel.add(polinomr);
		panel.add(polinomc);
		panel.add(txt_pol1);
		panel.add(txt_pol2);
		panel.add(txt_rezultat);
		panel.add(txt_rezultat_rest);
		
		// add buttons in label
		panel.add(adunare);	
		panel.add(scadere);
		panel.add(inmultire);
		panel.add(inmultireScalar);	
		panel.add(impartire);
		panel.add(derivare);	
		panel.add(integrare);
		
		// add panel in window
		frame.add(panel);
		frame.setVisible(true);
	}
	
	public static void main(String[] args){

		// exemplu de polinom 
		// 12x^4+6x^3+2x^1+7x^0
		// 3x^2-1x^1+2x^0
		
		new Main();
		
	}
	
	public class ActionEvents implements ActionListener{

		public void actionPerformed(ActionEvent event){
			
			Operatii o = new Operatii();
			String pol1 = txt_pol1.getText();
			String pol2 = txt_pol2.getText();
			
			if(event.getSource() == adunare){	
				o.spargere(pol1, pol2);
				txt_rezultat.setText(o.toString(o.adunare(o.polinom1, o.polinom2)));
			}
			if(event.getSource() == scadere){
				o.spargere(pol1, pol2);
				txt_rezultat.setText(o.toString(o.scadere(o.polinom1, o.polinom2)));
			}
			if(event.getSource() == inmultire){
				o.spargere(pol1, pol2);
				txt_rezultat.setText(o.toString(o.inmultire(o.polinom1, o.polinom2)));
			}
			if(event.getSource() == impartire){
				o.spargere(pol1, pol2);
				txt_rezultat.setText(o.toString(o.impartire(o.polinom1, o.polinom2)));
				txt_rezultat_rest.setText(o.toString(o.polinom1));
			}
			if(event.getSource() == inmultireScalar){
				int nr = Integer.parseInt(JOptionPane.showInputDialog(null, "introdu scalar", "", JOptionPane.PLAIN_MESSAGE));
				o.spargere1(pol2);
				txt_rezultat.setText(o.toString(o.inmultireCuScalar(nr, o.polinom2)));
			}
			if(event.getSource() == derivare){
				o.spargere1(pol2);
				txt_rezultat.setText(o.toString(o.derivare(o.polinom2)));
			}
			if(event.getSource() == integrare){
				o.spargere1(pol2);
				txt_rezultat.setText(o.toString2(o.integrare(o.polinom2)));
			}
		}
	}
}
